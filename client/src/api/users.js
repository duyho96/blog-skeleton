import config from '../config';

import { request } from '../services/api';

import * as myself from './users';
export default myself;

const baseEndpoint = `${config.apiBaseURL}/users`;

export function search({page}) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, {
    method: 'GET',
    data: {
      page
    }
  });
}

export function get(id) {
  const endpoint = `${baseEndpoint}/${id}`;
  return request(endpoint, {
    method: 'GET'
  });
}

export function update(id, data) {
  const endpoint = `${baseEndpoint}/${id}`;
  return request(endpoint, {
    method: 'PATCH',
    data
  });
}

export function create(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, {
    method: 'POST',
    data
  });
}

export function disable(id) {
  const endpoint = `${baseEndpoint}/${id}`;
  return request(endpoint, {
    method: 'DELETE'
  });
}
