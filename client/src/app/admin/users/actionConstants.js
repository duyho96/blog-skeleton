/**
 * This file is defined action constants
 */
export const CLEAR_USER_LIST = 'USERS:CLEAR_USER_LIST';
export const FETCH_USER_LIST = 'USERS:FETCH_USER_LIST';
export const SET_IS_USER_LOADED = 'USERS:SET_IS_USER_LOADED';
