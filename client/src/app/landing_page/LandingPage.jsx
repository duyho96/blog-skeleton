import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export class LandingPage extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div>
        <h1>Landing Page</h1>
      </div>
    )
  }
}

LandingPage.propTypes = {
  activeView: PropTypes.string.isRequired
}
LandingPage.displayName = 'LandingPage'

export default LandingPage
