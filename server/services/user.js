var UserModel = require('../models').user;

module.exports.getList = function (page, perPage = 3) {
    return UserModel.findAll({offset: (page - 1) * perPage, limit: perPage});
};
