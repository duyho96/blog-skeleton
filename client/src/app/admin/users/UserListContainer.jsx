import { connect } from 'react-redux'
import get from 'lodash/get'

import UserList from './UserList'

import { fetchUsers } from './actions'

export const mapStateToProps = (state, ownProps) => {
  const user = state.getIn(['admin', 'user']).toJS()
  const userList = get(user, 'userList')
  const isLoaded = get(user, 'isLoaded')

  return Object.assign({}, ownProps, {
    userList,
    isLoaded
  })
}

export const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchUsers: (params) => {
      dispatch(fetchUsers(params))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList)
