import React from 'react'
import PropTypes from 'prop-types'

import { hot } from 'react-hot-loader'

import { BrowserRouter, Route, Switch } from 'react-router-dom'

import AdminDashboardContainer from './admin/AdminDashboardContainer'
import LandingPageContainer from './landing_page/LandingPageContainer'

const renderAdminDashboardContainer = props => <AdminDashboardContainer {...props} />
const renderLandingPageContainer = props => <LandingPageContainer {...props} />

function App (props) {
  return (
    <BrowserRouter basename={props.basename}>
      <div>
        <Switch>
          <Route exact path='/' render={renderLandingPageContainer} />
          <Route exact path='/admin' render={renderAdminDashboardContainer} />
          <Route path='/admin/:view' render={renderAdminDashboardContainer} />
        </Switch>
      </div>
    </BrowserRouter>
  )
}

App.propTypes = {
  basename: PropTypes.string
}

export default hot(module)(App)
