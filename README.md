# This project is a sample skeleton structure for a Blog project

## Installation

#### Server (NodeJS with MySQL)
- Rename ./server/.env.example to .env and update the database connection info
- Init
```
    npm run build
    cd ./server
    node_modules/.bin/sequelize db:migrate
    node_modules/.bin/sequelize db:seed:all
```
- Start
```
    npm run start
    npm run devstart // with nodemon
```
- Sample API: [http://localhost:3000/users?page=1](http://localhost:3000/users?page=1)
- Dependencies:
    - [dotenv](https://github.com/motdotla/dotenv) 
    - [mysql2](https://github.com/brianmario/mysql2)
    - [sequelize](https://github.com/sequelize/sequelize)
    - [sequelize-cli](https://github.com/sequelize/cli)
    - [nodemon](https://github.com/remy/nodemon) 
#### Client (ReactJS)
- To be completed..
