/**
 * This file is managed all reducers of out application
 */
import Immutable from 'immutable'
import { combineReducers } from 'redux-immutable'

import adminReducers from './admin/reducer'

const appInitialState = Immutable.fromJS({
})

const appReducer = combineReducers(
  Object.assign({
    admin: adminReducers
  })
)

export default (state = appInitialState, action) => {
  const finalState = appReducer(state, action)

  return finalState
}
