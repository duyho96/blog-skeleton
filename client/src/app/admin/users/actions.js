import get from 'lodash/get'

import * as API from '../../../api/users';

import * as actionConstants from './actionConstants'

export function clearUserList () {
  return {
    type: actionConstants.CLEAR_USER_LIST
  }
}

export function setUserIsLoaded (data) {
  return {
    data,
    type: actionConstants.SET_IS_USER_LOADED
  }
}

export function fetchUsers (queryParams) {
  return (dispatch, getState) => {
    return API.search(queryParams)
      .then(response => {
        return {
          data: response,
          type: actionConstants.FETCH_USER_LIST
        }
      })
      .catch((error) => {
        console.log('>>>>>>>>', error)
        return {
          data: [],
          meta: {},
          type: actionConstants.FETCH_USER_LIST
        }
      })
      .then(searchResultAction => {
        dispatch(searchResultAction)
      })
  }
}
