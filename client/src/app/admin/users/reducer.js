import Immutable from 'immutable'

import * as actionTypes from './actionConstants'

const initialState = Immutable.fromJS({
  isLoaded: false,
  isLoadingMore: false,
  userList: []
})

export default function userReducer(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.FETCH_USER_LIST:

      if (action.data) {
        state = state.updateIn(['userList'], userList => userList.concat(Immutable.fromJS(action.data)))
      }

      state = state
        .set('isLoaded', true)
        .set('isLoadingMore', false)

      break

    default:
      break
  }

  return state
}
