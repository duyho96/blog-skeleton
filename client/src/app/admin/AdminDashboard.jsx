import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import UserListContainer from './users/UserListContainer'

export class AdminDashboard extends React.Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
  }

  componentDidUpdate (prevProps, prevState) {
  }

  componentWillUnmount () {
  }

  renderDashboard () {
    return (
      <div>
        <h1>Admin Dashboard</h1>
        <Link to='/admin/users'>Users</Link>
      </div>
    )
  }

  renderActiveView () {
    const { activeView } = this.props
    let view

    switch (activeView) {
      case 'users':
        view = <UserListContainer {...this.props} />
        break

      default:
        view = this.renderDashboard()
        break
    }

    return view
  }

  render () {
    return (
      <div>
        {this.renderActiveView()}
      </div>
    )
  }
}

AdminDashboard.propTypes = {
  activeView: PropTypes.string.isRequired
}
AdminDashboard.displayName = 'AdminDashboard'

export default AdminDashboard
