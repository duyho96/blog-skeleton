import React from 'react'
import PropTypes from 'prop-types'

export class UserList extends React.Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    const { fetchUsers } = this.props
    fetchUsers({})
    fetchUsers({page: 2})
  }

  renderUsers () {
    const { userList } = this.props
    const rows = []

    const data = userList.map(user => {
      rows.push(
        <tr key={user.id}>
          <td>{user.id}</td>
          <td>{user.name}</td>
        </tr>
      )
    })

    return (
      <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    )
  }

  render () {
    return (
      <div>
        <h1>User List</h1>
        {this.renderUsers()}
      </div>
    )
  }
}

UserList.propTypes = {
  isLoaded: PropTypes.bool,
  fetchUsers: PropTypes.func.isRequired,
  userList: PropTypes.array
}
UserList.displayName = 'UserList'

export default UserList
